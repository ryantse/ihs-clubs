{**
 * IHS Clubs Platform
 * HTML Base Page Template
 *
 * Copyright (c) 2013 Ryan Tse.
 * 
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 *}

<!DOCTYPE html>
<html lang="en">
	<head>
		{include file="head.tpl"}
		<title>{block name="title"}IHS Clubs{/block}</title>
	</head>
	<body>
		{include file="header.tpl"}
		{block name="content"}{/block}
		{include file="footer.tpl"}
	</body>
</html>
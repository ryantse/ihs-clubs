{**
 * IHS Clubs Platform
 * HTML Page Header Template
 *
 * Copyright (c) 2013 Ryan Tse.
 * 
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 *}

<div class="container">
	<div class="masthead">
		<ul class="nav nav-pills pull-right">
			<li><a href="{$base_url}/directory/">Club Directory</a></li>
			<li><a href="{$base_url}/about/">About</a></li>
			{if true == isset($logged_in) && true == $logged_in}
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="{$base_url}/account/">
						Account <b class="caret"></b>
					</a>
					<ul class="dropdown-menu">
						<li><a href="{$base_url}/account/profile/">Profile</a></li>
						{if true == isset($is_admin) && true == $is_admin}
							<li><a href="{$base_url}/admin/">Admin</a></li>
						{/if}
						<li><a href="{$base_url}/authentication/logout/">Logout</a></li>
					</ul>
				</li>
			{else}
				<li><a href="{$base_url}/authentication/">Login/Register</a></li>
			{/if}
		</ul>
		<h3><a class="muted" href="{$base_url}/"><img class="logo" src="{$base_url}/static/img/logo.png" alt="IHS Clubs" /></a></h3>
	</div>
</div>
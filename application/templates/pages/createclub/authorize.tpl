{**
 * IHS Clubs Platform
 * Create Club Authorization Page
 *
 * Copyright (c) 2013 Ryan Tse.
 * 
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 *}

{extends file="base.tpl"}

{block name="title"}
{$smarty.block.parent} - Create a Club
{/block}

{block name="content"}
<div class="page-title">
	<div class="container">
		<h2>Create a Club</h2>
	</div>
</div>
<div class="container">
	<h3>Authorization</h3>
	<p class="lead">
		Please enter in the authorization code you received.
	</p>
	<div class="alert alert-info">
		All clubs this current school year must have been preapproved by the club commissioners prior to registration on this site. If you do not currently have an authorization code, please refer to the the create a club page located <a href="{$base_url}/about/faq/">here</a> for more information regarding the registration process. For questions regarding this process, please <a href="{$base_url}/about/contact/">contact us</a>.
	</div>
	{if true == isset($authorize_error_message)}
		<div class="alert alert-error">
			<strong>{$authorize_error_header|default:"Authorization Failed"}</strong><br />
			{$authorize_error_message nofilter}
		</div>
	{/if}
	<form class="form-horizontal" method="POST" action="{$base_url}/create/authorize/">
		<div class="control-group">
			<label class="control-label" for="authorization_code">Authorization</label>
			<div class="controls">
				<input type="text" placeholder="" id="authorization_code" name="authorization_code" autocomplete="off" tabindex="1"></input>
			</div>
		</div>
		<div class="form-actions">
			<button type="submit" name="authorize" class="btn btn-primary" tabindex="2">Authorize</button>
		</div>
	</form>
</div>
{/block}
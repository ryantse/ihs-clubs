{**
 * IHS Clubs Platform
 * Create Club Authentication Page
 *
 * Copyright (c) 2013 Ryan Tse.
 * 
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 *}

{extends file="base.tpl"}

{block name="title"}
{$smarty.block.parent} - Create a Club
{/block}

{block name="content"}
<div class="page-title">
	<div class="container">
		<h2>Create a Club</h2>
	</div>
</div>
<div class="container">
	<h3>Authentication</h3>
	<p class="lead">
		In order to begin creating a club, please login to your account.
	</p>
	<a href="{$authentication_url}" class="btn btn-primary">Login to Account</a>
</div>
{/block}
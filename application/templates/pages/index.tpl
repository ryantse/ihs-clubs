{**
 * IHS Clubs Platform
 * Website Index Page
 *
 * Copyright (c) 2013 Ryan Tse.
 * 
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 *}

{extends file="base.tpl"}

{block name="customhead"}
<link rel="stylesheet" type="text/css" href="{$base_url}/static/css/index.css" />
{/block}

{block name="content"}
<div class="hero-unit">
	<div class="container">
		<ul class="row">
			<li class="span1 spacers"></li>
			<li class="span4">
				<a href="{$base_url}/discover/">
					<h3 class="title">Explore Clubs</h3>
					<i class="icon-globe"></i>
				</a>
			</li>
			<li class="span2 spacers"></li>
			<li class="span4">
				<a href="{$base_url}/create/">
					<h3 class="title">Create a New Club</h3>
					<i class="icon-group"></i>
				</a>
			</li>
			<li class="span1 spacers"></li>
		</ul>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="span8">
			<h2 class="upcoming-events"><i class="icon-calendar"></i> Upcoming Club Events</h2>
			<h4 class="muted">August 28th, 2013</h4>
				<b>Service Learning Club</b>
				<p>Join your friends and family on a super duper walkathon to raise money to buy pancakes for Julianne because she is starving.</p>
				<a href="{$base_url}/clubs/service-learning-club/events/1">More Information &hellip;</a>
				<hr>
				<b>Gardening Club</b>
				<p>Meet up in the P-wing to join friends in planting beautiful plants throughout the school.</p>
				<a href="{$base_url}/clubs/gardening-club/events/5">More Information &hellip;</a>
				<hr>
		</div>
		<div class="span4">
			<h3>Club Resources</h3>
		</div>
	</div>
</div>
{/block}
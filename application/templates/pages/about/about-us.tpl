{**
 * IHS Clubs Platform
 * Website About Page
 *
 * Copyright (c) 2013 Ryan Tse.
 * 
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 *}

{extends file="base.tpl"}

{block name="title"}
{$smarty.block.parent} - About
{/block}

{block name="customhead"}
{/block}

{block name="content"}
<div class="page-title">
	<div class="container">
		<h2>About</h2>
	</div>
</div>
<div class="container">
	<div class="tabbable tabs-left">
		<ul class="nav nav-tabs">
			<li class="active"><a href="{$base_url}/about/">About</a></li>
			<li><a href="{$base_url}/about/faq/">FAQ</a></li>
			<li><a href="{$base_url}/about/contact/">Contact Us</a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="about">
				<div class="thumbnail">
					<img src="{$base_url}/static/img/about-banner.jpeg" />
				</div>
				<br />
				<p class="lead">Welcome to Irvington High School's club website. First created in 2013 by Ryan Tse, this site is the central location for all clubs on campus. From this website, you can find all the latest informaiton about activities and events from every single club on campus.</p>
				<h3>2013-2014 Club Commissioners</h3>
				<div class="span6">
					<div class="row">
						<div class="span1">
							<img src="http://placekitten.com/70/70" alt="" />
						</div>
						<div class="span5">
							<p><strong>Elise Logan</strong></p>
							<p>My mission is ...</p>
							<p><a href="{$base_url}/about/contact/">Contact</a></p>
						</div>
					</div>
				</div>
				<div class="span6">
					<div class="row">
						<div class="span1">
							<img src="http://placekitten.com/70/70" alt="" />
						</div>
						<div class="span5">
							<p><strong>Kavitta Ghai</strong></p>
							<p>My mission is ...</p>
							<p><a href="{$base_url}/about/contact/">Contact</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
{/block}
{**
 * IHS Clubs Platform
 * Authentication Login Page
 *
 * Copyright (c) 2013 Ryan Tse.
 * 
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 *}

{extends file="base.tpl"}

{block name="title"}
{$smarty.block.parent} - Login
{/block}

{block name="customhead"}
<link rel="stylesheet" type="text/css" href="{$base_url}/static/css/login.css" />
{/block}

{block name="content"}
<div class="page-title">
	<div class="container">
		<h2>Login</h2>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="span6">
			{nocache}
				<h3 class="title">Existing User</h3>
				{if true == isset($login_error_message)}
					<div class="alert alert-error">
						<strong>{$login_error_header|default:"Unable to Login"}</strong><br />
						{$login_error_message nofilter}
					</div>
				{/if}
				<form class="form-horizontal" method="POST" action="{$base_url}/authentication/login/{$query_string}">
					<div class="control-group">
						<label class="control-label" for="email">Email</label>
						<div class="controls">
							<input type="email" placeholder="" id="email" name="email" {form_registration_text_restore field="email"} tabindex="1"></input>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="password">Password</label>
						<div class="controls">
							<input type="password" placeholder="" id="password" name="password" value="" tabindex="2"></input>
						</div>
					</div>
					<div class="form-actions">
						<button type="submit" name="login" class="btn btn-primary" tabindex="3">Login</button>
						<a href="{$base_url}/authentication/forgot/" class="btn pull-right">Forgot Password</a>
					</div>
				</form>
			{/nocache}
		</div>
		<div class="span6">
			<h3 class="title">New User</h3>
			<div class="lead">To begin creating an account, please select which of the following options identifies you best.</div>
			<div class="btn-group">
				<a href="{$base_url}/authentication/register/?type=student" class="btn btn-primary">Student</a>
				<a href="{$base_url}/authentication/register/?type=teacher" class="btn">Teacher</a>
			</div>
		</div>
	</div>
</div>
{/block}
{**
 * IHS Clubs Platform
 * Authentication Registration Confirmation Page
 *
 * Copyright (c) 2013 Ryan Tse.
 * 
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 *}

{extends file="base.tpl"}

{block name="title"}
{$smarty.block.parent} - Register
{/block}

{block name="customhead"}
<link rel="stylesheet" type="text/css" href="{$base_url}/static/css/register.css" />
{/block}

{block name="content"}
<div class="page-title">
	<div class="container">
		<h2>Registration</h2>
	</div>
</div>
<div class="container">
	<h3>Account Registration Successful</h3>
	<p class="lead">
		{if isset($requires_email) && $requires_email}
			A confirmation email has been sent to verify your account.
		{else}
			Congratulations, you are all set! Click <a href="{$base_url}/authentication/login/">here</a> to login to your account.
		{/if}
	</p>
</div>
{/block}
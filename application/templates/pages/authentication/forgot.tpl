{**
 * IHS Clubs Platform
 * Authentication Forgot Login Page
 *
 * Copyright (c) 2013 Ryan Tse.
 * 
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 *}

{extends file="base.tpl"}

{block name="title"}
{$smarty.block.parent} - Forgot
{/block}

{block name="customhead"}
<link rel="stylesheet" type="text/css" href="{$base_url}/static/css/forgot.css" />
<script type="text/javascript">
	var RecaptchaOptions = {
		theme : 'custom',
		custom_theme_widget: 'recaptcha_widget'
	};
</script>
{/block}

{block name="content"}
<div class="page-title">
	<div class="container">
		<h2>Account Recovery</h2>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="span6">
			<h3 class="title">Forgot Login</h3>
			<div class="lead">Please enter your email in the form on the right and we will send you a password reset link for your account. The email should arrive almost instantly, but please allow up to 10-15 minutes for the slower email services to catch up.</div>
			<a href="{$base_url}/authentication/login/" class="btn">Back to Login</a>
		</div>
		<div class="span6">
			<h3 class="title">Password Recovery</h3>
			{nocache}
				{if true == isset($recovery_error_message)}
					<div class="alert alert-error">
						<strong>{$recovery_error_header|default:"An Error Occurred"}</strong><br />
						{$recovery_error_message nofilter}
					</div>
				{/if}
				{if true == isset($recovery_success) && true == $recovery_success}
					<div class="alert alert-success">
						<strong>Recovery Sent</strong><br />
						For security reasons, please check your email to determine if there is an account associated with the email address you entered.
					</div>
				{/if}
				<form class="form-horizontal" method="POST" action="{$base_url}/authentication/forgot/">
					<div class="control-group">
						<label class="control-label" for="email">Email</label>
						<div class="controls">
							<input type="email" placeholder="" id="email" name="email" {if false == isset($recovery_success) || false == $recovery_success}{form_registration_text_restore field="email"}{/if} tabindex="1"></input>
						</div>
					</div>
					<div id="recaptcha_widget" style="display:none">
						<div class="control-group">
							<label class="control-label" for="recaptcha_response_field"><small>re</small>CAPTCHA</label>
							<div class="controls">
								<div id="recaptcha_image" class="thumbnail"></div>
							</div>
						</div>
						<div class="control-group">
							<div class="controls">
								<div class="input-append">
									<input type="text" id="recaptcha_response_field" name="recaptcha_response_field" class="input-recaptcha" tabindex="2"></input>
									<a class="btn" href="javascript:Recaptcha.reload()"><i title="Get a new CAPTCHA." class="icon-refresh"></i></a>
									<a class="btn recaptcha_only_if_audio" href="javascript:Recaptcha.switch_type('image')"><i title="Get an image CAPTCHA." class="icon-picture"></i></a>
									<a class="btn recaptcha_only_if_image" href="javascript:Recaptcha.switch_type('audio')"><i title="Get an audio CAPTCHA." class="icon-headphones"></i></a>
								</div>
							</div>
						</div>
					</div>
					<noscript>
						<iframe src="{$recaptcha_noscript_url}" height="300" width="500" frameborder="0"></iframe><br>
						<textarea name="recaptcha_challenge_field" rows="3" cols="40" tabindex="2"></textarea>
						<input type="hidden" name="recaptcha_response_field" value="manual_challenge" />
					</noscript>
					<div class="form-actions">
						<button type="submit" name="recover" class="btn btn-primary" tabindex="3">Recover</button>
					</div>
					<script type="text/javascript" src="{$recaptcha_url}"></script>
				</form>
			{/nocache}
		</div>
	</div>
</div>
{/block}
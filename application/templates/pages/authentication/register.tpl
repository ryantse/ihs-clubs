{**
 * IHS Clubs Platform
 * Authentication Registration Page
 *
 * Copyright (c) 2013 Ryan Tse.
 * 
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 *}

{extends file="base.tpl"}

{block name="title"}
{$smarty.block.parent} - Register
{/block}

{block name="customhead"}
<link rel="stylesheet" type="text/css" href="{$base_url}/static/css/register.css" />
{/block}

{block name="content"}
<div class="page-title">
	<div class="container">
		<h2>Registration</h2>
	</div>
</div>
<div class="container">
	{nocache}
		{if "student" == $page_display}
			<h3 class="title">Student Registration</h3>
		{elseif "teacher" == $page_display}
			<h3 class="title">Teacher Registration</h3>
			<div class="alert alert-info">
				Teacher accounts require approval from a systems administartor. Please allow up to 72 hours for your registration to be processed and verified.
			</div>
		{/if}
		{if true == isset($register_error_message)}
			<div class="alert alert-error">
				<strong>{$register_error_header|default:"Unable to Create Account"}</strong><br />
				{$register_error_message nofilter}
			</div>
		{/if}
		<form class="form-horizontal" method="POST" action="{$base_url}/authentication/register/?type={$page_display}">
			<div class="control-group">
				<label class="control-label" for="fullname">Full Name</label>
				<div class="controls">
					<input type="text" placeholder="" id="fullname" name="fullname" {form_registration_text_restore field="fullname"} tabindex="1"></input>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="displayname">Display Name <small>(Optional)</small></label>
				<div class="controls">
					<input type="text" placeholder="" id="displayname" name="displayname" {form_registration_text_restore field="displayname"} tabindex="2"></input>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="email">Email</label>
				<div class="controls">
					<input type="email" placeholder="" id="email" name="email" {form_registration_text_restore field="email"} tabindex="3"></input>
				</div>
			</div>
			{if "student" == $page_display}
				<div class="control-group">
					<label class="control-label" for="grade">Grade</label>
					<div class="controls">
						<select id="grade" name="grade" tabindex="4">
							<option value="Select a Grade">Select a Grade</option>
							<option value="9" {form_registration_select_restore field="grade" value="9"}>9th</option>
							<option value="10" {form_registration_select_restore field="grade" value="10"}>10th</option>
							<option value="11" {form_registration_select_restore field="grade" value="11"}>11th</option>
							<option value="12" {form_registration_select_restore field="grade" value="12"}>12th</option>
							<option value="0" {form_registration_select_restore field="grade" value="0"}>Prefer not to specify.</option>
						</select>
					</div>
				</div>
			{/if}
			<div class="control-group">
				<label class="control-label" for="password">Password</label>
				<div class="controls">
					<input type="password" placeholder="" id="password" name="password" value="" tabindex="5"></input>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="confirmpassword">Confirm Password</label>
				<div class="controls">
					<input type="password" placeholder="" id="confirmpassword" name="confirmpassword" value="" tabindex="6"></input>
				</div>
			</div>
			<div class="form-actions">
				<button type="submit" name="register" class="btn btn-primary" tabindex="7">Register</button>
			</div>
		</form>
	{/nocache}
</div>
{/block}
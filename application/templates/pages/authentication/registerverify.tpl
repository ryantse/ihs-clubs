{**
 * IHS Clubs Platform
 * Authentication Registration Confirmation Page
 *
 * Copyright (c) 2013 Ryan Tse.
 * 
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 *}

{extends file="base.tpl"}

{block name="title"}
{$smarty.block.parent} - Register
{/block}

{block name="content"}
<div class="page-title">
	<div class="container">
		<h2>Registration</h2>
	</div>
</div>
<div class="container">
	{if "success" == $page_display}
		<h3>Account Verification Successful</h3>
		<p class="lead">
			Congratulations, you are all set! Click <a href="{$base_url}/authentication/login/">here</a> to login to your account.
		</p>
	{elseif "failure" == $page_display}
		<h3>Oops!</h3>
		<div class="alert alert-error">
			We were unable to verify your email with the information provided. Please verify that the link is correct. If you continue to have problems, please <a href="{$base_url}/about/contact/">contact us</a>.
		</div>
	{/if}
</div>
{/block}
{**
 * IHS Clubs Platform
 * HTML Page Footer Template
 *
 * Copyright (c) 2013 Ryan Tse.
 * 
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 *}

<div class="container">
	<footer>
		<p class="pull-right"><a href="{$base_url}/about/privacy/">Privacy</a> &middot; <a href="{$base_url}/about/terms/">Terms</a></p>
		<p>&copy; 2013 Irvington High School &middot; Written by Ryan Tse.</p>
	</footer>
</div>
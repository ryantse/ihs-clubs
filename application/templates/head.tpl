{**
 * IHS Clubs Platform
 * HTML Page Head Template
 *
 * Copyright (c) 2013 Ryan Tse.
 * 
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 *}

<meta charset="UTF-8" />

<link rel="stylesheet" type="text/css" href="{$base_url}/static/css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="{$base_url}/static/css/global.css" />
<link rel="stylesheet" type="text/css" href="{$base_url}/static/css/font-awesome.css" />
<!--[if IE 7]>
	<link rel="stylesheet" type="text/css" href="{$base_url}/static/css/font-awesome-ie7.css" />
<![endif]-->
{block name="customhead"}{/block}
<link rel="stylesheet" type="text/css" href="{$base_url}/static/css/bootstrap-responsive.css" />

<script type="text/javascript" src="{$base_url}/static/js/jquery.min.js"></script>
<script type="text/javascript" src="{$base_url}/static/js/bootstrap.js"></script>
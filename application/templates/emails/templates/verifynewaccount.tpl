{**
 * IHS Clubs Platform
 * Email Verify New Account
 *
 * Copyright (c) 2013 Ryan Tse.
 * 
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 *}

{capture name="email_content_html" assign="email_content_html"}
Hello {$name},<br />
<br />
In order to verify your account, please click the following link: <a href="{$register_url}">Verify Account Now</a><br />
<br />
Thank you,<br />
IHS Clubs
{/capture}

{capture name="email_content_text" assign="email_content_text"}
Hello {$name},

In order to verify your account, please navigate to: {$register_url}.

Thank you,
IHS Clubs
{/capture}
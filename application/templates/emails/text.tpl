{**
 * IHS Clubs Platform
 * Email Text Template
 *
 * Copyright (c) 2013 Ryan Tse.
 * 
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 *}

{if true == isset($template_target)}
	{include file="emails/templates/$template_target.tpl"}
	{$smarty.capture.email_content_text}
{else}
	{$email_content_text}
{/if}
{**
 * IHS Clubs Platform
 * 404 Not Found Page
 *
 * Copyright (c) 2013 Ryan Tse.
 * 
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 *}

{extends file="base.tpl"}

{block name="title"}
{$smarty.block.parent} - 404 Not Found
{/block}

{block name="content"}
<div class="page-title">
	<div class="container">
		<h2>404 Not Found</h2>
	</div>
</div>
<div class="container">
	<div class="alert alert-error">
		The page you requested located at {$request_url} could not be found. The link may have expired or the resource no longer exists on the server. If you require any further support, please <a href="{$base_url}/about/contact/">contact us</a> in regards to this issue.
	</div>
</div>
{/block}
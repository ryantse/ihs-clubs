{**
 * IHS Clubs Platform
 * 403 Forbidden Page
 *
 * Copyright (c) 2013 Ryan Tse.
 * 
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 *}

{extends file="base.tpl"}

{block name="title"}
{$smarty.block.parent} - 403 Forbidden
{/block}

{block name="content"}
<div class="page-title">
	<div class="container">
		<h2>403 Forbidden</h2>
	</div>
</div>
<div class="container">
	<div class="alert alert-error">
		You do not have sufficient permissions to access {$request_url}. If you require any further support, please <a href="{$base_url}/about/contact/">contact us</a> in regards to this issue.
	</div>
</div>
{/block}
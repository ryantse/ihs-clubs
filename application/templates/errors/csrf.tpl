{**
 * IHS Clubs Platform
 * CSRF Forbidden Page
 *
 * Copyright (c) 2013 Ryan Tse.
 * 
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 *}

{extends file="base.tpl"}

{block name="title"}
{$smarty.block.parent} - 403 Forbidden (CSRF)
{/block}

{block name="content"}
<div class="page-title">
	<div class="container">
		<h2>403 Forbidden (CSRF)</h2>
	</div>
</div>
<div class="container">
	<div class="alert alert-error">
		A cross-site request forgery (CSRF) attack has been detected. Your request for the page located at {$request_url} has been cancelled. <b>This may have been triggered if you attempted to resubmit a form by refreshing the page.</b> If you require any further support, please <a href="{$base_url}/about/contact/">contact us</a> in regards to this issue.
	</div>
</div>
{/block}
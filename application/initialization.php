<?php

/**
 * IHS Clubs Platform
 * Application Initialization Script
 *
 * Copyright (c) 2013 Ryan Tse.
 * 
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

// Ensure that the initialization script is called bootstrapper.
if(!defined("__APPDIR__")) { die(); }

// Load required application libraries.
require(constant("__APPDIR__")."/core/libraries/Router/Router.class.php");
require(constant("__APPDIR__")."/core/libraries/Smarty/Smarty.class.php");
require(constant("__APPDIR__")."/core/libraries/Mailer/Mailer.class.php");
require(constant("__APPDIR__")."/core/libraries/Configuration/Configuration.class.php");
require(constant("__APPDIR__")."/core/libraries/reCAPTCHA/reCAPTCHA.class.php");

// Load application configuration.
require(constant("__APPDIR__")."/configuration/configuration.php");

// Load application controllers.
// TODO: require(constant("__APPDIR__")."/core/controllers/SecurityController.php");
require(constant("__APPDIR__")."/core/controllers/PagesController.php");
require(constant("__APPDIR__")."/core/controllers/UsersController.php");

// Load page routing table and begin routing.
require(constant("__APPDIR__")."/core/controllers/RoutingTable.php");
require(constant("__APPDIR__")."/core/controllers/RoutingDirector.php");

?>
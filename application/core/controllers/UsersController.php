<?php

/**
 * IHS Clubs Platform
 * User Account Controller
 *
 * Copyright (c) 2013 Ryan Tse.
 * 
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

final class User {
	private $properties;
	private $database;

	private $property_dictionary = array(
		"id" => "user_id",
		"status" => "user_status",
		"fullname" => "user_fullname",
		"displayname" => "user_displayname",
		"grade" => "user_grade",
		"email" => "user_email",
		"password" => "user_password"
	);

	const BLOWFISH_CIPHER     = "2a";
	const BLOWFISH_COMPLEXITY = 11;
	
	const FLAG_SUSPENDED    = 1;
	const FLAG_UNREGISTERED = 2;
	const FLAG_REGISTERED   = 4;
	const FLAG_STUDENT      = 8;
	const FLAG_TEACHER      = 16;
	const FLAG_COMMISSIONER = 32;
	const FLAG_SYSADMIN     = 64;
	const FLAG_AUTHORIZED   = 128;
	const FLAG_VERIFIED     = 256;

	final public function __construct($search_type, $search_query) {
		$search_dictionary = array(
			1 => "user_id",
			2 => "user_email"
		);

		// Do type conversion from integer to string.
		if(is_int($search_type)) {
			if($search_dictionary[intval($search_type)] == undefined) {
				throw new Exception();
			}

			$search_type = $search_dictionary[intval($search_type)];
		}

		// Make sure that the search type is a valid search.
		if(false == in_array($search_type, array_values($search_dictionary))) {
			throw new Exception();
		}

		// Check that the database is connected.
		$database_config = Configuration::open("DATABASE");
		if(true == isset($database_config->connected) && true == $database_config->connected && true == isset($database_config->connection)) {
			$this->database = $database_config->connection;
		}

		// Find the user id and make sure it exists.
		$user_query = $this->database->prepare("SELECT user_id FROM ".$database_config->prefix."users WHERE ".$search_type."=:query LIMIT 1");
		$user_query->execute(array(":query" => $search_query));
		$database_result = $user_query->fetch(PDO::FETCH_ASSOC);
		
		if(false == $database_result) {
			throw new Exception();
		} else {
			$this->properties = array();
			$this->properties["id"] = $database_result["user_id"];
		}
	}

	final public function __get($name) {
		// Get all permissible properties.
		$permissible_properties = array_keys($this->property_dictionary);

		// Remove restricted values.
		$restricted_values = array("status");
		$permissible_properties = array_diff($permissible_properties, $restricted_values);

		// Check if the value is restricted.
		if(false == in_array($name, $permissible_properties)) {
			throw new Exception();
		}

		return $this->FetchProperty($name);
	}

	final public function __set($name, $value) {
		// Get all permissible properties.
		$permissible_properties = array_keys($this->property_dictionary);

		// Remove restricted values.
		$restricted_values = array("id", "status", "password");
		$permissible_properties = array_diff($permissible_properties, $restricted_values);

		// Check if the value is restricted.
		if(false == in_array($name, $permissible_properties)) {
			throw new Exception();
		}

		$this->SetProperty($name, $value);
		$this->FetchProperty($name, false);
	}

	final public function PrefetchAll() {
		$database_config = Configuration::open("DATABASE");

		// Get all values from the database.
		$database_result = $this->database->query("SELECT ".implode(array_values($this->property_dictionary), ",")." FROM ".$database_config->prefix."users WHERE user_id=".$this->database->quote($this->properties["id"])." LIMIT 1");
		$database_result = $database_result->fetch(PDO::FETCH_ASSOC);

		// Store all the data in the array.
		foreach($this->property_dictionary as $property_id => $property_column) {
			$this->properties[$property_id] = $database_result[$property_column];
		}
	}

	final private function FetchProperty($property, $cached = true) {
		// Check if there is a definition for the property.
		if(false == isset($this->property_dictionary[$property])) {
			return undefined;
		}

		// Check if the value has already been retrieved and if it should ignore the cache.
		if(!isset($this->properties[$property]) || !$cached) {
			$database_config = Configuration::open("DATABASE");
			$database_result = $this->database->query("SELECT ".$this->property_dictionary[$property]." FROM ".$database_config->prefix."users WHERE user_id=".$this->database->quote($this->properties["id"])." LIMIT 1");
			if(false !== $database_result) {
				$this->properties[$property] = $database_result->fetchColumn(0);
			}
		}

		return $this->properties[$property];
	}

	final private function SetProperty($property, $value) {
		// Get all permissible properties.
		$permissible_properties = array_keys($this->property_dictionary);

		// Remove restricted values.
		$restricted_values = array("id");
		$permissible_properties = array_diff($permissible_properties, $restricted_values);

		// Check if the value is restricted.
		if(false == in_array($property, $permissible_properties)) {
			throw new Exception();
		}

		// Check if the value to be set is actually defined.
		if(false == isset($value)) {
			return;
		}

		// Set the value in the database.
		$database_config = Configuration::open("DATABASE");
		$database_result = $this->database->query("UPDATE ".$database_config->prefix."users SET ".$this->property_dictionary[$property]."=".$this->database->quote($value)." WHERE user_id=".$this->database->quote($this->id)." LIMIT 1");

		if(true == $database_result) {
			return true;
		}

		return false;
	}

	final private function GenerateSalt() {
		// Generate salt string.
		$salt_string = bin2hex(openssl_random_pseudo_bytes(CRYPT_SALT_LENGTH, $is_secure));

		// Check if salt was cryptographically secure.
		if(false == $is_secure) {
			$salt_string = "";
			$dictionary = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

			// Fall back method, much less secure, but better than nothing.
			for($i = 0; $i < 22; $i++) {
					$salt_string .= $dictionary[mt_rand(0, strlen($dictionary)-1)];
			}
		}

		$salt_string = substr($salt_string, 0, 22);
		return "\$".self::BLOWFISH_CIPHER."\$".self::BLOWFISH_COMPLEXITY."\$".$salt_string."\$";
	}

	final public function ComparePassword($current_password) {
		// Compare the current password stored to the value passed by function.
		if($this->FetchProperty("password") !== crypt(hash("sha256", $current_password), $this->FetchProperty("password", false))) {
			return false;
		}

		// Check if complexity has not been changed on the system.
		$check_complexity = "\$".self::BLOWFISH_CIPHER."\$".self::BLOWFISH_COMPLEXITY."\$";
		if(false === strpos($this->FetchProperty("password"), $check_complexity, 0)) {
			$this->ChangePassword($current_password);
		}

		return true;
	}

	final public function GetFlag($property_flag) {
		// Check the status of a flag.
		return (($this->FetchProperty("status") & $property_flag) == $property_flag);
	}

	final public function SetFlag($property_flag, $value) {
		$flags = $this->FetchProperty("status", false);

		// Check if the value should be set as true or false.
		if(true == $value) {
			$flags |= $property_flag;
		} else {
			$flags &= ~$property_flag;
		}

		$this->SetProperty("status", $flags);
	}

	final public function ToggleFlag($property_flag) {
		$this->SetFlag($property_flag, !($this->GetFlag($property_flag)));
	}

	final public function ChangePassword($new_password) {
		// Regenerate a new crypt value and generate a new salt.
		$this->SetProperty("password", crypt(hash("sha256", $new_password), $this->GenerateSalt()));
		$this->FetchProperty("password", false);

		// Check that the password was successfully set.
		return $this->ComparePassword($new_password);
	}
}

?>
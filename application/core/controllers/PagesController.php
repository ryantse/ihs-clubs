<?php

/**
 * IHS Clubs Platform
 * Page Template Controller
 *
 * Copyright (c) 2013 Ryan Tse.
 * 
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

class Pages {
	private $session_started = false;
	private $template_started = false;

	public function __construct($templating = true, $sessions = true, $database = true, $csrf = true) {
		$application_config = Configuration::open("APPLICATION");
		if($templating) $this->InitializeTemplates();
		if($database) $this->InitializeDatabase();
		if($sessions) $this->InitializeSession();
		if($csrf) $this->InitializeCSRF();

		if(true == $application_config->ssl) {
			header("Strict-Transport-Security: max-age=604800; includeSubDomains");
		}
		header("X-Frame-Options: DENY");
	}

	private function InitializeCSRF() {
		// Set CSRF post processing.
		$this->template->registerFilter("output", array($this, "CSRF_Protect"));

		// Verify CSRF tokens.
		$this->CSRF_Verify();
	}

	private function InitializeTemplates() {
		if(false == $this->template_started) {
			$application_config = Configuration::open("APPLICATION");
			$this->template = new Smarty();
			$this->template->setUseSubDirs(TRUE);

			// Temporarily force recompile of all templates.
			$this->template->force_compile = true;

			// HTML post processing.
			$this->template->registerFilter("output", array($this, "HTML_Compress"));

			// Set Smarty directories.
			$this->template->setTemplateDir(constant("__APPDIR__")."/templates/");
			$this->template->setCompileDir(constant("__APPDIR__")."/templates/compiled/");
			$this->template->setCacheDir(constant("__APPDIR__")."/templates/cached/");
			$this->template->setConfigDir(constant("__APPDIR__")."/templates/configs/");

			// Add plugins directory.
			$this->template->addPluginsDir(constant("__APPDIR__")."/core/libraries/Smarty/customplugins");

			$this->template_data = $this->template->createData();
			$this->template_data->assign("base_url", $application_config->url);
			if(0 < strlen($_SERVER["QUERY_STRING"])) {
				$this->template_data->assign("query_string", "?".$_SERVER["QUERY_STRING"]."&");
			} else {
				$this->template_data->assign("query_string", "");
			}
		}

		$this->template_started = true;
	}

	private function InitializeSession() {
		if(false == $this->session_started) {
			$application_config = Configuration::open("APPLICATION");

			session_save_path(constant("__APPDIR__")."/sessions");
			ini_set("session.gc_maxlifetime", 60 * 60 * 24 * 365);
			ini_set("session.gc_probability", 1);
			ini_set("session.gc_divisor", 1);

			session_set_cookie_params(time() + (60 * 60 * 24 * 365), "/", $_SERVER["HTTP_HOST"], $application_config->ssl, true);
			session_name(hash("sha512", hash("sha512", $application_config->security_token)."_SESSION_NAME"));
			session_start();
			setcookie(session_name(), session_id(), time() + (60 * 60 * 24 * 365), "/", $_SERVER["HTTP_HOST"], $application_config->ssl, true);

			if(true == isset($_SESSION["logged_in"]) && true == $_SESSION["logged_in"] && true == isset($_SESSION["user_id"])) {
				try {
					$this->user = new User("user_id", $_SESSION["user_id"]);
					$this->template_data->assign("logged_in", true);
					if(true == $this->user->GetFlag(User::FLAG_COMMISSIONER) || true == $this->user->GetFlag(User::FLAG_SYSADMIN)) {
						$this->template_data->assign("is_admin", true);
					}
				} catch (Exception $exception) {
					$_SESSION["logged_in"] = false;
					$_SESSION["user_id"] = 0;
					$_SESSION["unique_id"] = 0;

					unset($_SESSION["logged_in"]);
					unset($_SESSION["user_id"]);
					unset($_SESSION["unique_id"]);
				}
			}

			if(false == isset($_SESSION["unique_id"])) {
				$_SESSION["unique_id"] = uniqid("", true);
			}
		}

		$this->session_started = true;
	}

	private function InitializeDatabase() {
		$database_config = Configuration::open("DATABASE");
		try {
			$database_config->connection = new PDO($database_config->driver.":dbname=".$database_config->database.";host=".$database_config->host, $database_config->username, $database_config->password);
			$database_config->connected = true;
		} catch (PDOException $exception) {
			unset($database_config->connection);
			$database_config->connection_error = $exception->getMessage();
			$database_config->connected = false;
		}
	}

	public function getRedirect($destination, $hash_only = false, $return_type = "url") {
		$application_config = Configuration::open("APPLICATION");
		$this->InitializeSession();

		$hash = md5($application_config->security_token."_redirect_".$_SESSION["unique_id"]."_".$destination);
		if(0 < strlen($destination)) {
			if(true == $hash_only) {
				return $hash;
			} else if (false == $hash_only) {
				if("url" == $return_type) {
					return "redirect=".urlencode($destination)."&hash=".$hash;
				} else if ("array" == $return_type) {
					return array(
						"redirect" => $destination,
						"hash" => $hash
					);
				}
			}
		}
	}

	public function doRedirect($redirect = true, $stop = true) {
		$application_config = Configuration::open("APPLICATION");
		$this->InitializeSession();
		
		if(true == isset($_GET["redirect"]) && true == isset($_GET["hash"])) {
			$hash = md5($application_config->security_token."_redirect_".$_SESSION["unique_id"]."_".urldecode($_GET["redirect"]));
			if ($_GET["hash"] == $hash) {
				if(true == $redirect) {
					header("Location: ".$application_config->url.urldecode($_GET["redirect"]));
					if(true == $stop) {
						die();
					}
				} else {
					return urldecode($_GET["redirect"]);
				}
			}
		}
	}

	public function getDatabase() {
		$database_config = Configuration::open("DATABASE");
		if(false == isset($database_config->connected) || false == $database_config->connected) {
			$this->InitializeDatabase();
		}

		if(false == $database_config->connected) {
			return $database_config->connection_error;
		}

		return $database_config->connection;
	}

	private function CSRF_Verify() {
		if("POST" === $_SERVER["REQUEST_METHOD"]) {
			$this->InitializeSession();
			if(true == isset($_POST["CSRFKey"]) && true == isset($_POST["CSRFToken"]) && true == isset($_COOKIE["csrf_session"])) {
				$csrf_session = $_COOKIE["csrf_session"];
				// Make sure that the CSRF token is valid.
				if(true == isset($_SESSION["csrf_tokens"][$_POST["CSRFKey"]]["token"]) && $_POST["CSRFToken"] == $_SESSION["csrf_tokens"][$_POST["CSRFKey"]]["token"] && $_COOKIE["csrf_session"] == $_SESSION["csrf_tokens"][$_POST["CSRFKey"]]["session"]) {
					$is_xmlhttprequest = $_SESSION["csrf_tokens"][$_POST["CSRFKey"]]["xmlhttprequest"];

					// Destroy the token.
					if(false == $is_xmlhttprequest) {
						unset($_SESSION["csrf_tokens"][$_POST["CSRFKey"]]);
					}

					// Check if the request was a XMLHTTPRequest.
					if((true == isset($_SERVER["HTTP_X_REQUESTED_WITH"])) && ("xmlhttprequest" == strtolower($_SERVER["HTTP_X_REQUESTED_WITH"]))) {
						// Check if the request should have been an XMLHTTPRequest.
						if(true == $is_xmlhttprequest) {
							return;
						} else if(false == $is_xmlhttprequest) {
							// An attempt was made to use a POST CSRF token for XMLHTTPRequest.
							unset($_SESSION["csrf_tokens"][$_POST["CSRFKey"]]);
						}
					} else {
						if(false == $is_xmlhttprequest) {
							return;
						}
					}
				}
			}
		} else {
			return;
		}


		ob_end_clean();
		ob_start("ob_gzhandler");
		$application_config = Configuration::open("APPLICATION");
		$this->template_data->assign("request_url", $_SERVER["REQUEST_URI"]);
		header("HTTP/1.0 403 Forbidden");
		$this->template->display("errors/csrf.tpl", $this->template_data);
		exit();
	}

	public function CSRF_Protect($html, $template) {
		if(false == isset($_SESSION["csrf_tokens"]) || false == is_array($_SESSION["csrf_tokens"])) {
			$_SESSION["csrf_tokens"] = array();
		}

		if(false == isset($_COOKIE["csrf_session"])) {
			$csrf_session = hash("sha256", bin2hex(openssl_random_pseudo_bytes(20)));
			setcookie("csrf_session", $csrf_session);
		} else {
			$csrf_session = $_COOKIE["csrf_session"];
		}

		// Add CSRF to all forms.
		$count = preg_match_all("/<form(.*?)>(.*?)<\\/form>/is", $html, $matches, PREG_SET_ORDER);
		if (true == is_array($matches)) {
			foreach ($matches as $m) {
				if(false !== strpos($m[1], "nocsrf")) {
					continue;
				}
				$csrf_key = hash("sha256", bin2hex(openssl_random_pseudo_bytes(20)));
				$csrf_token = hash("sha256", bin2hex(openssl_random_pseudo_bytes(20)));
				$_SESSION["csrf_tokens"][$csrf_key] = array(
					"token" => $csrf_token,
					"xmlhttprequest" => false,
					"session" => $csrf_session
				);
				$html = str_replace($m[0], "<form".$m[1].">".$m[2]."<input type=\"hidden\" name=\"CSRFKey\" value=\"".$csrf_key."\" /><input type=\"hidden\" name=\"CSRFToken\" value=\"".$csrf_token."\" /></form>", $html);
			}
		}

		// Add Javascript support for CSRF.
		$csrf_key = hash("sha256", bin2hex(openssl_random_pseudo_bytes(20)));
		$csrf_token = hash("sha256", bin2hex(openssl_random_pseudo_bytes(20)));
		
		$_SESSION["csrf_tokens"][$csrf_key] = array(
			"token" => $csrf_token,
			"xmlhttprequest" => true,
			"session" => $csrf_session
		);
		$html = str_replace("<head>", "<head><script type=\"text/javascript\">var csrf_token=\"".$csrf_token."\"; var csrf_key=\"".$csrf_key."\";</script>", $html);

		return $html;
	}

	public function HTML_Compress($html, $template) {
		return preg_replace(array('/<!--(.*)-->/Uis',"/[[:blank:]]+/"),array('',' '),str_replace(array("\n","\r","\t"),'',$html));
	}

	public function page_forbidden() {
		ob_end_clean();
		ob_start("ob_gzhandler");
		$application_config = Configuration::open("APPLICATION");
		$this->template_data->assign("request_url", $_SERVER["REQUEST_URI"]);
		header("HTTP/1.0 403 Forbidden");
		$this->template->display("errors/403.tpl", $this->template_data);
	}

	public function page_notfound() {
		ob_end_clean();
		ob_start("ob_gzhandler");
		$application_config = Configuration::open("APPLICATION");
		$this->template_data->assign("request_url", $_SERVER["REQUEST_URI"]);
		header("HTTP/1.0 404 Not Found");
		$this->template->display("errors/404.tpl", $this->template_data);
	}
}

?>
<?php

/**
 * IHS Clubs Platform
 * URL Routing Table
 *
 * Copyright (c) 2013 Ryan Tse.
 *
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

// Platform home page.
Router::add("/", "landing/index");

// Git pages.
Router::add("/git-post/", "git/request");

// About pages.
Router::add("/about/", "about/index");
Router::add("/about/faq/", "about/faq");
Router::add("/about/contact/(:any)", "about/contact");

// Authentication pages.
Router::add("/authentication/", "authentication/login");
Router::add("/authentication/logout/", "authentication/logout");
Router::add("/authentication/login/", "authentication/login");
Router::add("/authentication/register/", "authentication/register");
Router::add("/authentication/register/verify/(:any)", "authentication/registerverify/$1");
Router::add("/authentication/forgot/", "authentication/forgot");
Router::add("/authentication/forgot/verify/(:any)", "authentication/forgotverify/$1");
Router::add("/authentication/claim/(:any)", "authentication/claim/$1");
Router::add("/authentication/claim/verify/(:any)", "authentication/claimverify/$1");

// Account pages.
Router::add("/account/", "account/profile");
Router::add("/account/settings/", "account/settings");
Router::add("/account/profile/", "account/profile");

// Admin pages.
Router::add("/admin/", "admin/index");

// Club directory.
Router::add("/directory/", "directory/index");

// Club pages.
Router::add("/create/(:any)", "clubs/create/$1");
Router::add("/clubs/(:any)", "clubs/index/$1");

// Catch all for pages not match previous queries, should be last at all times.
Router::add("/(:any)", "landing/page_notfound");

?>
<?php

/**
 * IHS Clubs Platform
 * Routing Parser and Handler Initialization
 *
 * Copyright (c) 2013 Ryan Tse.
 *
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

$url = $_SERVER["REQUEST_URI"];

if (strpos( $_SERVER["REQUEST_URI"], '?') !== false) {
	$qs = '?'.parse_url($_SERVER["REQUEST_URI"], PHP_URL_QUERY);
	$url = str_replace($qs, '', $_SERVER["REQUEST_URI"]);
}

if ($url[strlen($url)-1] !== "/") {
	$url = $url."/";
}

$url_parts = explode("/",Router::route($url));
@list($class, $function) = $url_parts;
$class = ucfirst($class);

ob_start("ob_gzhandler");

try {
	if(file_exists(constant("__APPDIR__")."/core/handlers/".$class.".php")) {
		require(constant("__APPDIR__")."/core/handlers/".$class.".php");
		$controller = new $class;
		if($function == "") {
			$function = "index";
			array_shift($url_parts);
		} else {
			array_shift($url_parts);
			array_shift($url_parts);
		}
		if(method_exists($controller, $function) == TRUE) {
			call_user_func_array(array($controller, $function), $url_parts);
		} else {
			throw new BadFunctionCallException();
		}
	} else {
		throw new BadFunctionCallException();
	}
} catch (BadFunctionCallException $exception) {
	die("An unknown error occurred. Please try again later.");
}

?>
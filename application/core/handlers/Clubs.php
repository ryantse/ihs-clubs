<?php

/**
 * IHS Clubs Platform
 * Clubs Page Handler
 *
 * Copyright (c) 2013 Ryan Tse.
 * 
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

class Clubs extends Pages {
	public function __construct() {
		//Load templates, session, and database.
		parent::__construct();
	}

	public function create() {
		$application_config = Configuration::open("APPLICATION");

		switch(func_get_arg(0)) {
			case "authenticate":
				if(false == isset($_SESSION["logged_in"]) || false == $_SESSION["logged_in"]) {
					$authentication_url = $application_config->url."/authentication/login/?".parent::getRedirect("/create/authenticate/");
					$this->template_data->assign("authentication_url", $authentication_url);
					$this->template->display("pages/createclub/authenticate.tpl", $this->template_data);
				} else {
					header("Location: ".$application_config->url."/create/authorize/");
				}
				break;

			case "authorize":
				// Make sure the user is logged in.
				if(false == isset($_SESSION["logged_in"]) || false == $_SESSION["logged_in"]) {
					header("Location: ".$application_config->url."/create/authenticate/");
				}

				// Check if the authorization code is set.
				if(true == isset($_SESSION["authorization_code"]) && 0 < strlen($_SESSION["authorization_code"])) {
					header("Location: ".$application_config->url."/create/new/");
				}

				if(true == isset($_POST["authorize"])) {
					$database_config = Configuration::open("DATABASE");
					$database = parent::getDatabase();
					while (true == true) {
						// Find the user id and make sure it exists.
						$user_query = $database->prepare("SELECT club_id FROM ".$database_config->prefix."clubs WHERE club_authorization=:code LIMIT 1");
						$user_query->execute(array(":code" => $_POST["authorization_code"]));
						$database_result = $user_query->fetch(PDO::FETCH_ASSOC);
						
						if(false == $database_result) {
							$this->template_data->assign("authorize_error_message", "The authorization code could not be verified. Please verify that you have entered the correct code.");
							break;
						}

						$_SESSION["club_id"] = $database_result["club_id"];
						$_SESSION["authorization_code"] = $_POST["authorization_code"];
						header("Location: ".$application_config->url."/create/new/");
						break;
					}
				}
				$this->template->display("pages/createclub/authorize.tpl", $this->template_data);
				break;

			case "new":
				// Make sure the user is logged in.
				if(false == isset($_SESSION["logged_in"]) || false == $_SESSION["logged_in"]) {
					header("Location: ".$application_config->url."/create/authenticate/");
				}

				// Make sure the authorization code is set.
				if(false == isset($_SESSION["authorization_code"]) || 0 == strlen($_SESSION["authorization_code"])) {
					header("Location: ".$application_config->url."/create/authorize/");
				}
				break;

			default:
				header("Location: ".$application_config->url."/create/authenticate/");
				break;
		}
	}
}

?>
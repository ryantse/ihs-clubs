<?php

/**
 * IHS Clubs Platform
 * About Page Handler
 *
 * Copyright (c) 2013 Ryan Tse.
 * 
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

class About extends Pages {
	public function __construct() {
		//Load templates, session, CSRF, and database.
		parent::__construct();
	}

	public function index() {
		$this->template->display("pages/about/about-us.tpl", $this->template_data);
	}

	public function faq() {
		$this->template->display("pages/about/faq.tpl", $this->template_data);
	}

	public function contactus() {
		$this->template->display("pages/about/contact-us.tpl", $this->template_data);
	}
}

?>
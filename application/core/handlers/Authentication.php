<?php

/**
 * IHS Clubs Platform
 * Authentication Page Handler
 *
 * Copyright (c) 2013 Ryan Tse.
 * 
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

class Authentication extends Pages {
	public function __construct() {
		//Load templates, session, CSRF, and database.
		parent::__construct();
	}

	public function login() {
		$application_config = Configuration::open("APPLICATION");

		// Check if there is already a user logged in.
		if(true == isset($_SESSION["logged_in"]) && true == $_SESSION["logged_in"]) {
			header("Location: ".$application_config->url."/account/");
		}

		// Check if it is actually a login routine.
		if(true == isset($_POST["login"])) {
			while(true == true) {
				// Check if the necessary values are set.
				if(false == isset($_POST["email"]) || false == isset($_POST["password"]) || "" == $_POST["email"] || "" == $_POST["password"]) {
					$this->template_data->assign("login_error_message", "Please enter your email and password.");
					break;
				}

				try {
					// Fetch the login information from the database.
					$user = new User("user_email", $_POST["email"]);
				} catch (Exception $exception) {
					unset($user);
					$this->template_data->assign("login_error_message", "Unable to locate an account with the specified email and password. If you forgot your password, please click <a href=\"".$application_config->url."/authentication/forgot/\">here</a> to recover your account.");
					break;
				}

				break;
			}

			if(true == isset($user)) {
				while(true == true) {
					// Check if the account is active.
					if(true == $user->GetFlag(User::FLAG_UNREGISTERED)) {
						$this->template_data->assign("login_error_header", "Unregistered Account");
						$this->template_data->assign("login_error_message", "This email has already been used to register for a club, click <a href=\"".$application_config->url."/authentication/claim/".urlencode($_POST["email"])."\">here</a> to begin the account setup process.");
						break;
					}

					// Check if the account has been verified.
					if(false == $user->GetFlag(User::FLAG_VERIFIED)) {
						$this->template_data->assign("login_error_header", "Verify Account");
						$this->template_data->assign("login_error_message", "This account has not been verified yet. Please check your email and verify your account.");
						break;
					}

					// Check if the account's password is valid.
					if(false == $user->ComparePassword($_POST["password"])) {
						$this->template_data->assign("login_error_message", "Unable to locate an account with the specified email and password. If you forgot your password, please click <a href=\"".$application_config->url."/authentication/forgot/\">here</a> to recover your account.");
						break;
					}

					// Check if the account is suspended.
					if(true == $user->GetFlag(User::FLAG_SUSPENDED)) {
						$this->template_data->assign("login_error_header", "Account Suspended");
						$this->template_data->assign("login_error_message", "This account has been suspended by an administrator. If you believe this is an error, please <a href=\"".$application_config->url."/about/contact/\">contact us</a>.");
						break;
					}

					// Check if the account is a teacher account and is authorized yet.
					if(true == $user->GetFlag(User::FLAG_TEACHER) && false == $user->GetFlag(User::FLAG_AUTHORIZED)) {
						$this->template_data->assign("login_error_header", "Account Pending Authorization");
						$this->template_data->assign("login_error_message", "This account is still pending authorization from an administrator. Please allow up to 72 hours for approval of a new account.");
						break;
					}

					//The user has managed to authenticate.
					$_SESSION["logged_in"] = true;
					$_SESSION["user_id"] = $user->id;
					session_regenerate_id(true);
					parent::doRedirect();
					header("Location: ".$application_config->url."/account/");
					break;
				}
			}
		} else {
			$this->template_data->assign("login_email", "");
		}

		// Show login page to the user.
		$this->template->display("pages/authentication/login.tpl", $this->template_data);
	}

	public function register() {
		$application_config = Configuration::open("APPLICATION");

		// Check if there is already a user logged in.
		if(true == isset($_SESSION["logged_in"]) && true == $_SESSION["logged_in"]) {
			header("Location: ".$application_config->url."/account/");
		}

		// Set default page type.
		$this->template_data->assign("page_type", "student");

		// Check if the get type is set.
		if(false == isset($_GET["type"])) {
			$_GET["type"] = "student";
		}

		// Check if there is a setting for the page.
		switch($_GET["type"]) {
			case "student":
				if(true == isset($_POST["register"])) {
					try {
						$user = new User("user_email", $_POST["email"]);
					} catch (Exception $exception) {
						unset($user);
					}

					while(true == true) {
						if(true == isset($user)) {
							if(true == $user->GetFlag(User::FLAG_REGISTERED)) {
								$this->template_data->assign("register_error_header", "Unable to Create Account");
								$this->template_data->assign("register_error_message", "An account already exists with this email address. If you forgot your password, please reset it <a href=\"".$application_config->url."/authentication/forgot/\">here</a>.");
								break;
							} else {
								$this->template_data->assign("register_error_header", "Unregistered Account");
								$this->template_data->assign("register_error_message", "This email has already been partially setup, click <a href=\"".$application_config->url."/authentication/claim/".urlencode($_POST["email"])."\">here</a> to begin the account claim process.");
								break;
							}
						}

						$account_error_strings = array();

						// Check if the full name at least contains more than two characters.
						if(2 > strlen(trim($_POST["fullname"]))) {
							array_push($account_error_strings, "Please specify your full name.");
						}

						// Check if display name is being set as a blank string.
						if("" !== $_POST["displayname"] && 2 > strlen(trim($_POST["displayname"]))) {
							array_push($account_error_strings, "Your display name cannot be blank.");
						}

						// Check for valid email address.
						if(false == (filter_var($_POST["email"], FILTER_VALIDATE_EMAIL) && preg_match('/@.+\./', $_POST["email"]))) {
							array_push($account_error_strings, "The email you specified is invalid.");
						}

						// Specify acceptable grade levels.
						$acceptable_grades = array("9", "10", "11", "12", "0");
						if(false == in_array($_POST["grade"], $acceptable_grades)) {
							array_push($account_error_strings, "Please select your grade.");
						}

						// Check that the passwords are of acceptable length and match with confirmation.
						while(true == true) {
							if(5 > strlen($_POST["password"])) {
								array_push($account_error_strings, "Your password must at least be 5 characters long.");
								break;
							}

							if($_POST["password"] !== $_POST["confirmpassword"]) {
								array_push($account_error_strings, "The passwords you entered did not match.");
								break;
							}

							break;
						}

						// Check if any errors occurred.
						if(0 < count($account_error_strings)) {
							$error_message_string = "The following errors were encountered when trying to create your account: <ul>";
							for($i = 0; $i < count($account_error_strings); $i++) {
								$error_message_string = $error_message_string."<li>".$account_error_strings[$i]."</li>";
							}
							$error_message_string = $error_message_string."</ul>";
							$this->template_data->assign("register_error_message", $error_message_string);
							break;
						}

						// Register user after having passed all checks.
						$database_config = Configuration::open("DATABASE");
						$application_config = Configuration::open("APPLICATION");
						$email_config = Configuration::open("EMAIL");

						$database = parent::getDatabase();
						$prepared_statement = $database->prepare("INSERT INTO ".$database_config->prefix."users (user_status, user_fullname, user_displayname, user_grade, user_email) VALUES (:status, :fullname, :displayname, :grade, :email)");
						$user_data = array();

						$user_data[":status"] = User::FLAG_REGISTERED | User::FLAG_STUDENT;
						$user_data[":fullname"] = $_POST["fullname"];
						$user_data[":grade"] = $_POST["grade"];
						$user_data[":email"] = $_POST["email"];

						if(true == isset($_POST["displayname"]) && "" !== $_POST["displayname"] ) {
							$user_data[":displayname"] = $_POST["displayname"];
						} else {
							$user_data[":displayname"] = $_POST["fullname"];
						}

						$prepared_statement->execute($user_data);
						$new_user = new User("user_id", $database->lastInsertId());
						$new_user->ChangePassword($_POST["password"]);

						$mailer = new PHPMailer(true);

						$email_data = $this->template->createData();
						$email_data->assign("name", $user_data[":displayname"]);
						$email_data->assign("email", $user_data[":email"]);
						$email_data->assign("register_url", $application_config->url."/authentication/register/verify/".urlencode($_POST["email"])."/".md5($new_user->id.$new_user->email.$application_config->security_token));
						$email_data->assign("template_target", "verifynewaccount");

						$mailer->Body = trim($this->template->fetch("emails/html.tpl", $email_data));
						$mailer->AltBody = trim($this->template->fetch("emails/text.tpl", $email_data));
						$mailer->XMailer = $email_config->name;
						$mailer->AddAddress($email_data->getTemplateVars('email'), $email_data->getTemplateVars('name'));
						$mailer->SetFrom($email_config->email, $email_config->name);
						$mailer->Subject = "IHS Clubs Activation";
						$mailer->Send();

						$this->template_data->assign("requires_email", true);
						$this->template->display("pages/authentication/confirm.tpl", $this->template_data);

						return;
					}
				}
				$this->template_data->assign("page_display", "student");
				break;

			case "teacher":
				if(true == isset($_POST["register"])) {
					try {
						$user = new User("user_email", $_POST["email"]);
					} catch (Exception $exception) {
						unset($user);
					}

					while(true == true) {
						if(true == isset($user)) {
							if(true == $user->GetFlag(User::FLAG_REGISTERED)) {
								$this->template_data->assign("register_error_message", "An account already exists with this email address. If you forgot your password, please reset it <a href=\"".$application_config->url."/authentication/forgot/\">here</a>.");
								break;
							} else {
								$this->template_data->assign("register_error_header", "Unregistered Account");
								$this->template_data->assign("register_error_message", "This email has already been partially setup, please click <a href=\"".$application_config->url."/authentication/claim/".urlencode($_POST["email"])."\">here</a> to begin the account claim process.");
								break;
							}
						}

						$account_error_strings = array();

						// Check if the full name at least contains more than two characters.
						if(2 > strlen(trim($_POST["fullname"]))) {
							array_push($account_error_strings, "Please specify your full name.");
						}

						// Check if display name is being set as a blank string.
						if("" !== $_POST["displayname"] && 2 > strlen(trim($_POST["displayname"]))) {
							array_push($account_error_strings, "Your display name cannot be blank.");
						}

						// Check for valid email address.
						if(false == (filter_var($_POST["email"], FILTER_VALIDATE_EMAIL) && preg_match('/@.+\./', $_POST["email"]))) {
							array_push($account_error_strings, "The email you specified is invalid.");
						}

						// Check that the passwords are of acceptable length and match with confirmation.
						while(true == true) {
							if(5 > strlen($_POST["password"])) {
								array_push($account_error_strings, "Your password must at least be 5 characters long.");
								break;
							}

							if($_POST["password"] !== $_POST["confirmpassword"]) {
								array_push($account_error_strings, "The passwords you entered did not match.");
								break;
							}

							break;
						}

						// Check if any errors occurred.
						if(0 < count($account_error_strings)) {
							$error_message_string = "The following errors were encountered when trying to create your account: <ul>";
							for($i = 0; $i < count($account_error_strings); $i++) {
								$error_message_string = $error_message_string."<li>".$account_error_strings[$i]."</li>";
							}
							$error_message_string = $error_message_string."</ul>";
							$this->template_data->assign("register_error_message", $error_message_string);
							break;
						}

						// Register user after having passed all checks.
						$database_config = Configuration::open("DATABASE");
						$application_config = Configuration::open("APPLICATION");
						$email_config = Configuration::open("EMAIL");

						$database = parent::getDatabase();
						$prepared_statement = $database->prepare("INSERT INTO ".$database_config->prefix."users (user_status, user_fullname, user_displayname, user_grade, user_email) VALUES (:status, :fullname, :displayname, :grade, :email)");
						$user_data = array();

						$user_data[":status"] = User::FLAG_REGISTERED | User::FLAG_TEACHER;
						$user_data[":fullname"] = $_POST["fullname"];
						$user_data[":grade"] = 0;
						$user_data[":email"] = $_POST["email"];

						if(true == isset($_POST["displayname"]) && "" !== $_POST["displayname"] ) {
							$user_data[":displayname"] = $_POST["displayname"];
						} else {
							$user_data[":displayname"] = $_POST["fullname"];
						}

						$prepared_statement->execute($user_data);
						$new_user = new User("user_id", $database->lastInsertId());
						$new_user->ChangePassword($_POST["password"]);

						$mailer = new PHPMailer(true);
						
						$email_data = $this->template->createData();
						$email_data->assign("name", $user_data[":displayname"]);
						$email_data->assign("email", $user_data[":email"]);
						$email_data->assign("register_url", $application_config->url."/authentication/register/verify/".urlencode($_POST["email"])."/".md5($new_user->id.$new_user->email.$application_config->security_token));
						$email_data->assign("template_target", "verifynewaccount");						
						
						$mailer->Body = trim($this->template->fetch("emails/html.tpl", $email_data));
						$mailer->AltBody = trim($this->template->fetch("emails/text.tpl", $email_data));
						$mailer->XMailer = $email_config->name;
						$mailer->AddAddress($email_data->getTemplateVars('email'), $email_data->getTemplateVars('name'));
						$mailer->SetFrom($email_config->email, $email_config->name);
						$mailer->Subject = "IHS Clubs Activation";
						$mailer->Send();

						$this->template_data->assign("requires_email", true);
						$this->template->display("pages/authentication/confirm.tpl", $this->template_data);
						return;
					}
				}
				$this->template_data->assign("page_display", "teacher");
				break;
		}

		// Display the registration page to the user.
		$this->template->display("pages/authentication/register.tpl", $this->template_data);
	}

	public function registerverify() {
		$application_config = Configuration::open("APPLICATION");
		try {
			$user = new User("user_email", urldecode(func_get_arg(0)));
			if(func_get_arg(1) == md5($user->id.$user->email.$application_config->security_token)) {
				$user->SetFlag(User::FLAG_VERIFIED, true);
				$this->template_data->assign("page_display", "success");
			} else {
				$this->template_data->assign("page_display", "failure");
			}
		} catch (Exception $exception) {
			$this->template_data->assign("page_display", "failure");
		}

		$this->template->display("pages/authentication/registerverify.tpl", $this->template_data);
	}

	public function forgot() {
		$application_config = Configuration::open("APPLICATION");
		$captcha_config = Configuration::open("CAPTCHA");

		// Set the default values for the CAPTCHA URL.
		$this->template_data->assign("recaptcha_url", constant("RECAPTCHA_API_SECURE_SERVER")."/challenge?k=".$captcha_config->public_key);
		$this->template_data->assign("recaptcha_noscript_url", constant("RECAPTCHA_API_SECURE_SERVER")."/noscript?k=".$captcha_config->public_key);

		// Check if the user is submitting a recovery.
		if(true == isset($_POST["recover"])) {
			while(true == true) {
				$captcha_response = recaptcha_check_answer($captcha_config->private_key, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
				if(true == $captcha_response->is_valid) {
					if(false == (filter_var($_POST["email"], FILTER_VALIDATE_EMAIL) && preg_match('/@.+\./', $_POST["email"]))) {
						$this->template_data->assign("recovery_error_message","Please enter a valid email for recovery.");
					} else {
						try {
							// Attempt to access data from the user based upon the email provided.
							$user = new User("user_email", $_POST["email"]);
							$recovery_url = $application_config->url."/authentication/forgot/verify/".urlencode($_POST["email"])."/".hash("sha256", $user->password);
							// TODO: Send out email with recovery link.
							$this->template_data->assign("recovery_success", true);
						} catch (Exception $exception) {}
					}
					break;
				} else {
					$this->template_data->assign("recovery_error_message","The CAPTCHA value you entered could not be verified.");
					break;
				}
				break;
			}
		}

		$this->template->display("pages/authentication/forgot.tpl", $this->template_data);
	}

	public function logout() {
		$application_config = Configuration::open("APPLICATION");

		// Reset all session variables.
		$_SESSION["logged_in"] = false;
		$_SESSION["user_id"] = 0;

		// Destroy the session.
		session_destroy();

		// Redirect back to the authentication page.
		header("Location: ".$application_config->url."/authentication/");
	}
}

?>
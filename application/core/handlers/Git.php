<?php

/**
 * IHS Clubs Platform
 * Git Handler
 *
 * Copyright (c) 2013 Ryan Tse.
 * 
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

class Git extends Pages {
	public function __construct() {
		// Disable any loading engines.
		parent::__construct(false, false, false, false);
	}

	public function request() {
		$git_config = Configuration::open("GIT");

		// Check authentication key.
		if($git_config->authentication_key == $_GET["authorization"]) {
			$file = fopen($git_config->request_directory.$git_config->domain, "w+");
			fclose($file);
		}
	}
}
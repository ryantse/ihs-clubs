<?php

/**
 * IHS Clubs Platform
 * Landing Page Handler
 *
 * Copyright (c) 2013 Ryan Tse.
 * 
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

class Landing extends Pages {
	public function __construct() {
		//Load templates, session, CSRF, and database.
		parent::__construct();
	}

	public function index() {
		//Display index page.
		$this->template->display("pages/index.tpl", $this->template_data);
	}
}

?>
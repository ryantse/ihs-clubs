<?php

/**
 * IHS Clubs Platform
 * Form Select Restore
 *
 * Copyright (c) 2013 Ryan Tse.
 * 
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

function smarty_function_form_registration_select_restore($params, $smarty) {
	if(isset($_POST[$params["field"]]) && $_POST[$params["field"]] == $params["value"]) {
		return "selected=\"selected\"";
	}
}

?>
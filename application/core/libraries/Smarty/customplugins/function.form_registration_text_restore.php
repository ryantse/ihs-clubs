<?php

/**
 * IHS Clubs Platform
 * Form Text Restore
 *
 * Copyright (c) 2013 Ryan Tse.
 * 
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

function smarty_function_form_registration_text_restore($params, $smarty) {
	if(isset($_POST[$params["field"]])) {
		return "value=\"".htmlentities($_POST[$params["field"]])."\"";
	}
}

?>
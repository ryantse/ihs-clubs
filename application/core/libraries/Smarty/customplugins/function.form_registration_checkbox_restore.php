<?php

/**
 * IHS Clubs Platform
 * Form Checkbox Restore
 *
 * Copyright (c) 2013 Ryan Tse.
 * 
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

function smarty_function_form_registration_checkbox_restore($params, $smarty) {
	if(isset($_POST[$params["field"]]) && intval($_POST[$params["field"]]) == 1) {
		return "checked=\"checked\"";
	}
}

?>
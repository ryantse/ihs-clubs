<?php

/**
 * IHS Clubs Platform
 * Application Configuration
 *
 * Copyright (c) 2013 Ryan Tse.
 *
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

// Basic application configuration.
$application                  = Configuration::open("APPLICATION");
$application->url             = "https://ihsclubs.com";
$application->ssl             = true;
$application->security_token  = "sk1SxVzHGt8CxDrOqZl2qjxA6sTly68L0s5sEXICPN2ZchYo1zqULcNkEVXteUt";

// CAPTCHA security configuration.
$captcha                      = Configuration::open("CAPTCHA");
$captcha->enabled             = true;
$captcha->public_key          = "6LfQiuUSAAAAAC3PKpwwdFNpm7XTyDZRrCEgvmyh";
$captcha->private_key         = "6LfQiuUSAAAAAK_3RSLpkgSdevU8U_w_oYp41Jn7";

// Database configuration.
$database                     = Configuration::open("DATABASE");
$database->driver             = "mysql";
$database->host               = "localhost";
$database->username           = "ihs_clubs";
$database->password           = "TzNd8jRCl8nyMjuOvuNDtiekPUBj1AI5jZ1k388igYb3@#$%^&*(OW9CFFfJf5EAbe321M60Y!@#$%^&*()MevkZl5nehA7gWRG7SLa36WbYFEwI3X8{}:><><>~rgMYABmrdB2gjb6tL5vbF0UkVbP1W";
$database->database           = "ihs_clubs";
$database->prefix             = "ihsclubs_";

// Mailer configuration.
$email                        = Configuration::open("EMAIL");
$email->name                  = "IHS Clubs";
$email->email                 = "no-reply@ihsclubs.com";
$email->dkim                  = true;
$email->dkim_key              = constant("__APPDIR__")."/configuration/dkim-internal.key";

// Forwarder configuration.
$forwarder                    = Configuration::open("FORWARDER");
$forwarder->allow_attachments = true;
$forwarder->dkim              = true;
$forwarder->dkim_key          = constant("__APPDIR__")."/configuration/dkim-forwarder.key";

// GIT configuration.
$git                          = Configuration::open("GIT");
$git->authentication_key      = "MHiSOIGLsxe22ms7aEBGnGeaTLguPBXpHLgESqdWCKpUkxlizxsrgg8tJDqcYCA";
$git->request_directory       = "/opt/git-pull/requests/";
$git->domain                  = "ihsclubs.com";

// TODO: Dropbox integration.

// TODO: Facebook integration.

?>
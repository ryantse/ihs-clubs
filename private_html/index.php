<?php

/**
 * IHS Clubs Platform
 * Application Bootstrap Script
 *
 * Copyright (c) 2013 Ryan Tse.
 *
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

// Target application loading version, variable has no effect on application runtime.
define("__STAGING__", true);

if(defined("__STAGING__") && (constant("__STAGING__") == true)) {
	// Change if directory is different for staging.
	define("__APPDIR__", "/srv/www/ihsclubs.com/application");
} else {
	// Application directory should NOT have trailing slash.
	define("__APPDIR__", "/srv/www/ihsclubs.com/application");
}

// Load application from application directory.
require(constant("__APPDIR__")."/initialization.php");

?>